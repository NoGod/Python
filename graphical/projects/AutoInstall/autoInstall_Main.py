#fine tuning: input checker for critical messages and interruption of installation
#fine tuning: put ' ' on img path
#default prefix: DRI_PRIME=1 wine64
#resourcess save location problem
#spaced name problem. resove by using underscores
#permissions problem
#Uninstall feature remove: Exec, Icon, /usr/share/applications entry

from autoInstall import Ui_Install
from PyQt5 import QtWidgets, QtGui, QtCore
import subprocess

class installWindow(QtWidgets.QWidget, Ui_Install):

    def  __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.ui = Ui_Install()
        self.setupUi(self)
        
        self.ImageBtn.clicked.connect(self.ImageBrowse)
        self.PathBtn.clicked.connect(self.PathBrowse)
        self.InstallBtn.clicked.connect(self.install)

    def ImageBrowse(self):
        filename, _ = QtWidgets.QFileDialog.getOpenFileName(None, "Select Image", "", "Image Files(*.png *.jpg *.jpeg *.bmp)")
        self.imageBrowseFile = filename
        
        if filename:
            pixmap = QtGui.QPixmap(filename)
            pixmap = pixmap.scaled(self.ImageLbl.width(), self.ImageLbl.height(), QtCore.Qt.KeepAspectRatio)
            self.ImageLbl.setPixmap(pixmap)
            self.ImageLbl.setAlignment(QtCore.Qt.AlignCenter)

    def PathBrowse(self):
        filename, _ = QtWidgets.QFileDialog.getOpenFileName(None, "Select Executable file", "")
        if filename:
            self.Path_Edit.setText(filename)

    def fileTypeExtractor(self, string):
        return string.split('.')[len(string.split('.'))-1]

    def writeDesktop(self):        
        ImgPath = self.imageBrowseFile

        if(self.copyToResource_chk.isChecked()):
            filetype = self.fileTypeExtractor(ImgPath)
            ImgPath = f'/home/nogod/Programs/Execs/resources/{self.Name_Edit.text()}.{filetype}'
            subprocess.run(f'cp {self.imageBrowseFile} \'{ImgPath}\'', shell=True)
        else:
            pass

        desktop_sample = f'''[Desktop Entry]
Type=Application
Exec=\'/home/nogod/Programs/Execs/{self.Name_Edit.text()}.sh\'
Icon={ImgPath}
Comment={self.Desc_Edit.text()}
Terminal=false
Name={self.Name_Edit.text()}'''
        #text above is the desktop sample
        desktop_file = open(f'/usr/share/applications/{self.Name_Edit.text()}.desktop', 'w+')
        desktop_file.write(desktop_sample)

    def writeScript(self):
        script_file = open(f'/home/nogod/Programs/Execs/{self.Name_Edit.text()}.sh', 'w+')
        
        if(any(self.Prefix_Edit.text()) == False):
            newpath_noEXE = self.Path_Edit.text().rsplit('/',1)[0]
            script_file.write(f'cd {newpath_noEXE}'+'\n')
            script_file.write(f'DRI_PRIME=1 wine64 \'{self.Path_Edit.text()}\'')
        else:
            script_file.write(f'{self.Prefix_Edit.text()} \'{self.Path_Edit.text()}\'')
        subprocess.run(f'sudo chmod 777 \'/home/nogod/Programs/Execs/{self.Name_Edit.text()}.sh\'', shell=True)

    def clear(self):
        self.Name_Edit.clear()
        self.Prefix_Edit.clear()
        self.Desc_Edit.clear()
        self.Path_Edit.clear()
        self.ImageLbl.clear()
        
    def install(self):
        subprocess.run(f'sudo touch \'/usr/share/applications/{self.Name_Edit.text()}.desktop\' \'/home/nogod/Programs/Execs/{self.Name_Edit.text()}.sh\'', shell=True)
        self.writeDesktop()
        self.writeScript()
        self.clear()
        QtWidgets.QMessageBox.information(self, '', 'Installation successful!')

if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    widget = installWindow()
    widget.show()
    app.exec()

