# Diccs needs verification
# randomsupply looks like dirty implementation
# give functionality to menu buttons

from PyQt5 import QtCore, QtGui, QtWidgets
import random
from random import randint

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(447, 290)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(420, 70, 41, 31))
        font = QtGui.QFont()
        font.setFamily("Bitstream Vera Sans")
        font.setPointSize(11)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.comboBox_From = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox_From.setGeometry(QtCore.QRect(50, 70, 131, 31))
        self.comboBox_From.setObjectName("comboBox_From")
        self.usrInput_Disp = QtWidgets.QLineEdit(self.centralwidget)
        self.usrInput_Disp.setGeometry(QtCore.QRect(50, 150, 151, 31))
        self.usrInput_Disp.setObjectName("usrInput_Disp")
        self.SwapBtn = QtWidgets.QPushButton(self.centralwidget)
        self.SwapBtn.setGeometry(QtCore.QRect(190, 70, 80, 31))
        self.SwapBtn.setObjectName("SwapBtn")
        self.comboBox_To = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox_To.setGeometry(QtCore.QRect(280, 70, 131, 31))
        self.comboBox_To.setObjectName("comboBox_To")
        self.submitBtn = QtWidgets.QPushButton(self.centralwidget)
        self.submitBtn.setGeometry(QtCore.QRect(180, 200, 101, 41))
        font = QtGui.QFont()
        font.setFamily("Comic Sans MS")
        font.setPointSize(17)
        self.submitBtn.setFont(font)
        self.submitBtn.setObjectName("submitBtn")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(10, 70, 41, 31))
        font = QtGui.QFont()
        font.setFamily("Bitstream Vera Sans")
        font.setPointSize(11)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.usrOutput_Disp = QtWidgets.QLabel(self.centralwidget)
        self.usrOutput_Disp.setGeometry(QtCore.QRect(260, 150, 151, 31))
        self.usrOutput_Disp.setFrameShape(QtWidgets.QFrame.Box)
        self.usrOutput_Disp.setFrameShadow(QtWidgets.QFrame.Plain)
        self.usrOutput_Disp.setText("")
        self.usrOutput_Disp.setObjectName("usrOutput_Disp")
        self.comboBox_Quantity = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox_Quantity.setGeometry(QtCore.QRect(10, 20, 91, 21))
        font = QtGui.QFont()
        font.setFamily("Bitstream Vera Sans")
        font.setPointSize(10)
        self.comboBox_Quantity.setFont(font)
        self.comboBox_Quantity.setObjectName("comboBox_Quantity")
        self.comboBox_Decimal = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox_Decimal.setGeometry(QtCore.QRect(360, 20, 79, 21))
        self.comboBox_Decimal.setEditable(True)
        self.comboBox_Decimal.setObjectName("comboBox_Decimal")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 447, 20))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        self.menuEdit = QtWidgets.QMenu(self.menubar)
        self.menuEdit.setObjectName("menuEdit")
        self.menuAbout = QtWidgets.QMenu(self.menubar)
        self.menuAbout.setObjectName("menuAbout")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionExit = QtWidgets.QAction(MainWindow)
        self.actionExit.setObjectName("actionExit")
        self.menuFile.addAction(self.actionExit)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuEdit.menuAction())
        self.menubar.addAction(self.menuAbout.menuAction())
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(290, 20, 61, 21))
        self.label_2.setObjectName("label_2")
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.submitBtn.setEnabled(False)
        self.usrInput_Disp.textChanged.connect(self.unfader)
        self.comboBox_Quantity.activated.connect(EnCov.comboTrigger)
        self.SwapBtn.clicked.connect(self.swapped)
        self.submitBtn.clicked.connect(EnCov.convert)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label_3.setText(_translate("MainWindow", "To"))
        self.SwapBtn.setText(_translate("MainWindow", "Swap"))
        self.submitBtn.setText(_translate("MainWindow", "Submit"))
        self.label.setText(_translate("MainWindow", "From"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.menuEdit.setTitle(_translate("MainWindow", "Edit"))
        self.menuAbout.setTitle(_translate("MainWindow", "About"))
        self.actionExit.setText(_translate("MainWindow", "Exit"))
        self.label_2.setText(_translate("MainWindow", "Decimals"))

    def swapped(self):
        comboBoxStr_1 = self.comboBox_From.currentText()
        comboBoxStr_2 = self.comboBox_To.currentText()
        index1 = self.comboBox_From.findText(comboBoxStr_1, QtCore.Qt.MatchFixedString)
        index2 = self.comboBox_To.findText(comboBoxStr_2, QtCore.Qt.MatchFixedString)
        self.comboBox_From.setCurrentIndex(index2)
        self.comboBox_To.setCurrentIndex(index1)

    def unfader(self):
        self.submitBtn.setEnabled(True)

class Engineering_conversion(object):

    def initialize(self):
        Quantity_Index = random.randint(0, (len(Quantities)-1))
        Quantity_Name = str(Quantities[Quantity_Index])
        Unit_Dicc = list(self.Dicc_Evaluator(Quantity_Name))
        self.cycleSupply(Quantities, 'comboBox_Quantity')
        self.cycleSupply(Unit_Dicc, 'comboBox_From')
        self.cycleSupply(Unit_Dicc, 'comboBox_To')
        ui.comboBox_Quantity.setCurrentIndex(Quantity_Index)
        self.randomSupply(Unit_Dicc)
        self.Decimal()
    def cycleSupply(self, Dicc_List, ComboName): # supplies texts for comboboxes
        for i in Dicc_List:
            eval('ui.'+f'{ComboName}').addItem(i)

    def Dicc_Evaluator(self, itemName):
        return eval(f'{itemName}'+'_Dicc')

    def comboTrigger(self):
        quantText = ui.comboBox_Quantity.currentText()
        quantText_Dicc = self.Dicc_Evaluator(quantText)
        ui.comboBox_From.clear()
        ui.comboBox_To.clear()
        self.cycleSupply(quantText_Dicc, 'comboBox_From')
        self.cycleSupply(quantText_Dicc, 'comboBox_To')
        self.randomSupply(quantText_Dicc)

    def randomSupply(self, DiccName):
        deegits = 0
        for i in DiccName:
            deegits += 1

        while(True):
            randInit_From = random.randint(0, (deegits-1))
            randInit_To = random.randint(0, (deegits-1))
            if(randInit_From == randInit_To):
                continue
            else:
                break
        ui.comboBox_From.setCurrentIndex(randInit_From)
        ui.comboBox_To.setCurrentIndex(randInit_To)
    def Decimal(self):
        for i in range(0, 10):
            decimal = f'0.{i}'
            ui.comboBox_Decimal.addItem(decimal)
        ui.comboBox_Decimal.setCurrentIndex(2)

    def convert(self):
        currentDicc = ui.comboBox_Quantity.currentText()
        comboBoxStr_1 = str(ui.comboBox_From.currentText())
        comboBoxStr_2 = str(ui.comboBox_To.currentText())
        inputVal = float(ui.usrInput_Disp.text())
        equiVal_From = float(self.Dicc_Evaluator(currentDicc)[comboBoxStr_1])
        equiVal_To = float(self.Dicc_Evaluator(currentDicc)[comboBoxStr_2])
        outputVal = (inputVal / equiVal_From) * equiVal_To
        decimalNum = ui.comboBox_Decimal.currentText() + 'f'
        ui.usrOutput_Disp.setText((format(outputVal, decimalNum)))

Quantities = [
    'Pressure',
    'Distance',
    'Energy'
    ]

Pressure_Dicc = {
    "atm": 1,
    "psi": 14.6959487755134,
    "mmH2o": 10332.274527999,
    "mH2o": 10.332274527999,
    "mmHg": 760,
    "Pa": 101325,
    "kPa": 101.325,
    "mPa": 0.101325,
    "bar": 1.01325,
    "Torr": 760
    }

Energy_Dicc = {
    'J': 1,
    'kJ': 1000,
    'Wh': 0.000277778,
    'kWh': 0.000000277778,
    'eV': 6.241509 * (10**18),
    'keV': 6.241509 * (10**15),
    'meV': 6.241509 * (10**12),
    'cal': 0.2388458966275,
    'kcal': 0.0002388458966275
    }

Distance_Dicc = {
    'm': 1,
    'mm': 1000,
    'cm': 100,
    'km': .001,
    'Mm': 1 /1000000,
    'Gm': 1 / 1000000000,
    'ft': 3.280839895,
    'mi': 0.000621371192,
    'yard': 1.09361,
    'Lightyear': 1 / 9460730777119564,
    'in': 1 / 254
    }

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    EnCov = Engineering_conversion()
    ui.setupUi(MainWindow)
    EnCov.initialize()
    MainWindow.show()
    sys.exit(app.exec_())
