#has to deal with multiple files
#search wheter file ends with .desktop or .sh
#might use .split()
#introduce function that will determine the format.

#writer.py accepts arguments whether sh or desktop argument
#has functions to write appropriate contents
#inherits from autoinstall to check whether chkbox has been triggered


from sys import argv
import subprocess

script, filename = argv
target = open(filename, 'r+')

class touch(QtWidgets.QWidget, Ui_Install):
    
    def  __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
    
    def touchwrite(self):
        desktop_sample = f'''[Desktop Entry]
Type=Application
Exec=/home/nogod/Programs/Execs/{self.Name_Edit.text()}.sh
Icon=/home/nogod/Programs/Execs/resources/{self.Name_Edit.text()}.png
Comment={self.Desc_Edit.text()}
Terminal=false
Name={self.Name_Edit.text()}'''
        target.write(desktop_sample)

touchy = touch()
touchy.touchwrite()