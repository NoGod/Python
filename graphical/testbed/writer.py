#.split()[1] = desktop possibility
from sys import argv
import subprocess
from autoInstall_Main import Ui_Install
from PyQt5 import QtWidgets, QtGui, QtCore
script, filename = argv
file = open(filename, 'r+')

class writer(Ui_Install):
    #def  __init__(self, *args, **kwargs):
        #super().__init__(*args, **kwargs)
    
    def writeDesktop(self):
        desktop_sample = f'''[Desktop Entry]
Type=Application
Exec=/home/nogod/Programs/Execs/{self.Name_Edit.text()}.sh
Icon=/home/nogod/Programs/Execs/resources/{self.Name_Edit.text()}.png
Comment={self.Desc_Edit.text()}
Terminal=false
Name={self.Name_Edit.text()}'''
        file.write(desktop_sample)

    def writeScript(self): # if prefix is present: use prefix otherwise default. needs detection of text absence
        if(self.Prefix_Edit.text() == None):
            file.write(f'DRI_PRIME=1 wine64 {Ui_Install.Path_Edit.text()}')
        else:
            file.write(f'{self.Prefix_Edit.text()} {self.Path_Edit.text()}')

Appwriter = writer()
if(str(file).split()[1] == 'desktop'):
    Appwriter.writeDesktop()
else:
    Appwriter.writeScript()