#This program arranges texts in files in alphabetical order and removes duplicates
from sys import argv

script, filename = argv
target = open(filename, 'r+')

def writer(listType):
    for i in range(len(listType)):
        target.write(listType[i])
        target.write('\n')

lines_list = target.read().splitlines()  # loads the text lines into list form
usrChoice = input('Process all? (y or n): ')

if(usrChoice == 'n'):
    upperLimit = int(input('Enter the upper limit: '))-1 # should input last protected line at top
    lowerLimit = int(input('Enter the lower limit: '))  # should input line first protected line at bottom
else:
    upperLimit = 0
    lowerLimit = len(lines_list)
    
upper_list = lines_list[:upperLimit] 
lower_list = lines_list[lowerLimit:] 

cut_list = lines_list[upperLimit:lowerLimit]
cut_list.sort()
lineNum2 = 0 # theres a difference between a line number and index number

while (lineNum2 != (len(cut_list)-1)):
    if(cut_list[lineNum2] == cut_list[(lineNum2+1)]):
        cut_list.remove(cut_list[(lineNum2+1)])
        continue
    else:
        lineNum2 += 1
        continue

target = open(filename, 'w+')
target.truncate()

writer(upper_list)
writer(cut_list)
writer(lower_list)