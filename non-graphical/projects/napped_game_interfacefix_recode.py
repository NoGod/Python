import sys
import random
import textwrap
from textwrap import dedent
from sys import exit

#new bug in corridor
#lockpick will be used to open lv room to get 'key'
#turn based system in future development

class misc:

    prompt = '>>> '
    line = '___________________________________________________________________________________________________________________________________________\n'
    separator = '######################################################'
    spacer = '\n'
    double_spacer = '\n\n'
    cheatCode = '1'
    void = 'not in the choices'

    def interfacer(self, upperPart, bottomPart):

        if(upperPart == None):
            print(f'{self.spacer}\n{self.line}\n{bottomPart}\n{self.spacer}')
        elif(bottomPart == None):
            print(f'{self.spacer}\n{self.line}\n{upperPart}\n{self.spacer}')
        else:
            print(f'{self.spacer}\n{self.line}\n{upperPart}\n{self.spacer}\n{bottomPart}\n{self.spacer}')

    def class_namer(self, jumbledMess):  #this will be solely used by passcodeLock for now
        cleanName = str((((str(jumbledMess)).split('.', 1))[1].split(' ', 1))[0])
        return cleanName


class inventory:

    items = []

    def check_items(self):

        if(self.items == [] or self.items == None):
            misc().interfacer('You have no items', None)
        else:
            misc().interfacer(f'You have {self.items}', None)

    def checker(self, item):   #can be used by other methods for item verification.

        if(item in self.items):
            return True
        else:
            return False

    def item_adder(self, additional_item):
        self.items.append(additional_item)

    def item_remover(self, itemToRemove):
        if(itemToRemove in self.items):
            self.items.remove(itemToRemove)
        else:
            print(f'You have no {itemToRemove}')

    def itemNumber_displayer(self):
        itemtext = ''
        for num in range(0, (len(inventory.items))):
            itemtext += ('({})'.format(num+1)+inventory.items[num]+' ')
        return itemtext

    def inventory_LenCounter(self):
        lenItems = []
        for i in range(0, len(self.items)):
            lenItems.append('{}'.format(i+1))
        return lenItems

    def list_subtractor(self, list1, list2): # list1 - list2
        while(list2 != []):
            if(list2[0] in list1):
                list1.remove(list2[0])
                list2.remove(list2[0])
            else:
                continue
        return list1

class passcodeLock:

    def __init__(self, digits, tolerance, digit_range, combination, proceedString, endString, classname, addItem):
        self.digits = int(digits)
        self.tolerance = int(tolerance)
        self.digit_range = int(digit_range)
        self.combination = str(combination)
        self.proceedString = proceedString
        self.endString = endString
        self.classname = classname
        self.addItem = str(addItem)

    def good_guess(self):
        deegits = self.digits
        randNum = f'{random.randint(0, self.digit_range)}'

        while((deegits-1) != 0):
            randNum_plus = f'{random.randint(0, self.digit_range)}'
            randNum += randNum_plus
            deegits -= 1

        return randNum

    def engage(self):

        text = f'This is a combination lock that requires {self.digits} digits with range (0-{self.digit_range}) and tolerance of {self.tolerance} tries. Type "back" to continue searching.'
        choices = f'Enter {self.digits} numbers:'
        misc().interfacer(text, choices)
        good_guess = self.good_guess()

        while(self.tolerance != 0):
            choice = input(f'{misc.prompt}')

            if((choice == self.combination) or (choice == good_guess) or (choice == misc.cheatCode)):
                print('Congrats!')
                passString = (str(misc().class_namer(self.classname)))+'_pass'
                maps.area_status[passString] = True
                if(self.addItem != None):
                    inventory().item_adder(self.addItem)
                (maps(self.proceedString, 'maps').mapper()).enter()
                break

            elif((choice == 'back') or (choice == 'Back')):
                self.classname.enter()

            else:
                self.tolerance -= 1

            if(self.tolerance == 0):
                (maps(self.endString, 'maps').mapper()).enter()
                break

    def engage2(self): # very dirty solely used by living room

        text = f'This is a combination lock that requires {self.digits} digits with range (0-{self.digit_range}) and tolerance of {self.tolerance} tries. Type "back" to continue searching.'
        choices = f'Enter {self.digits} numbers:'
        misc().interfacer(text, choices)
        good_guess = self.good_guess()

        while(self.tolerance != 0):
            choice = input(f'{misc.prompt}')

            if((choice == self.combination) or (choice == good_guess) or (choice == misc.cheatCode)):
                print('Congrats!')
                passString = (str(misc().class_namer(self.classname)))+'_pass'
                maps.area_status[passString] = True
                if(self.addItem != None):
                    inventory().item_adder(self.addItem)
                (maps(self.proceedString, 'maps').mapper()).enter()
                break

            elif((choice == 'back') or (choice == 'Back')):
                self.classname.after()

            else:
                self.tolerance -= 1

            if(self.tolerance == 0):
                (maps(self.endString, 'maps').mapper()).after()
                break
#-------------------------------------------------------------------------------------------------------------------------------------------------

class garage:

    def enter(self):
        print(f'You\'ve been kidnapped. The kidnappers went away and left you inside the van while you are tied in a rope. The rope seems breakable if you just have a way to cut it. You stopped panicking and just focused on finding a way to get out.{misc.double_spacer}As you look around, there seems to be some bottles of beer that was left by the kidnappers. What would you do?')
        print(f'{misc.spacer}[(1) smash bottle] [(2) nothing] {misc.spacer}')


        choice = input(misc.prompt)
        if((choice == 'smash bottle') or (choice == '1')):
            text = 'You smashed the bottle into shards and you used it to cut the rope. You can now get out of the van.'
            choices = '[(1) open door]'
            misc().interfacer(text, choices)

            choice = input(misc.prompt)
            if((choice == 'open door') or (choice == '1')):
                self.before_toolroom()

            else:
                print(f'{misc.void}\n{misc.line}')
                garage().enter()

        elif((choice == 'nothing') or (choice == '2')):
            print('You did nothing.\n')
            garage().enter()

        #for testing purposes only
        elif(choice == 'c'):
            inventory.items = ['poison', 'hammer', 'pliers', 'screwdriver', 'knife', 'smokeGrenade', 'chair', 'lockpick', 'lockpick', 'toaster', 'PoisonSleepingGrenade']
            maps.area_status['napper1_dead'] = True
            corridor().enter()

        else:
            garage().enter()

    def before_toolroom(self):

        text = 'As you get out of the van, you saw the garage door and the door leading inside the house.'
        choices = '[(1) Garage Door] [(2) House Door]'

        if((maps('garage_pass', 'area_status').mapper()) == True): # if area has been passed, only the choices will appear.
            misc().interfacer(None, choices)

        else:
            misc().interfacer(text, choices) # if area hasn't been passed, the all will appear.
            maps.area_status['garage_pass'] = True

        choice = input(misc.prompt)

        if((choice == 'Garage Door') or (choice == '1')):
            self.garageDoor()

        elif((choice == 'House Door') or (choice == '2')):

            text = 'You walked toward the door and you made sure not to make a noise. After that, you placed your ears on the door and it seems that there is no one in the room.'
            choices = '[(1) open door]'
            misc().interfacer(text,choices)

            choice = input(f'{misc.prompt}')

            if((choice == '1') or (choice == 'open door')):
                toolroom().enter()

            else:
                print(f'{misc.void}')
                self.enter()

        else:
            print(f'{misc.void}')
            self().before_toolroom()

    def garageDoor(self):

        text = 'You checked the Garage door and tried to open it but it is locked. It needs a key to open.'
        choices = '[(1) use key] [(2) use lockpick] [(3) go back]'
        misc().interfacer(text, choices)
        choice = input(misc.prompt)

        if((choice == 'use key') or (choice == '1')):
            if(inventory().checker('key') == True):
                misc().interfacer('You finally unlocked the garage door. Congratulations!', None)
                maps.area_status['good_ending'] = True
                GameOver().enter()

            else:
                text = 'You have no key.'
                misc().interfacer(text, None)
                self.garageDoor()

        elif((choice == 'use lockpick') or (choice == '2')):
            if(inventory().checker('lockpick') == True):
                text = 'This lock cannot be picked.'
                misc().interfacer(text, None)
                self.garageDoor()

            else:
                text = 'You have no lockpick.'
                misc().interfacer(text, None)
                self.garageDoor()

        elif((choice == 'go back') or (choice == '3')):
            self.before_toolroom()

        else:
            print(f'{misc.void}')
            self.garageDoor()

class toolroom:

    def enter(self):
        text_header1 = '[You are in the Tool Room]'
        text = f'[You are now in the toolroom]{misc.double_spacer}It is quiet inside the toolroom and you looked around to find some things that might help you escape. As you look around you saw poison packs, a picture frame, a toolbox and a locked door.{misc.double_spacer}What would you do?{misc.spacer}'
        choices = '[(1) take poison packs] [(2) inspect picture frame] [(3) inspect toolbox] [(4) open door] [(5) go back] [(6) check items]'

        if(maps.area_status['toolroom_pass'] == True):
            misc().interfacer(text_header1, choices)
        else:
            misc().interfacer(text, choices)
            maps.area_status['toolroom_pass'] = True

        choice = input(f'{misc.prompt}')

        if((choice == 'take poison packs') or (choice == '1')):
            text = f'You looked at the poison label and you saw a warning. Some text are missing.{misc.double_spacer}"This poison is highly toxic to humans. It contains **********, ********** , diphacinONE, bromadiolONE & stricNINE. Keep out of the reach of children. If swallowed, seek professional assistance or contact a poison control center immediately." This poison pack has only three sachets left'
            text_header1 = '[You already took the poison pack]'
            text_header2 = '[You took the poison pack]'

            if(((maps('toolroomRatpoison_taken', 'area_status')).mapper()) == True):
                misc().interfacer(text_header1, text)
                self.enter()

            else:
                misc().interfacer(text_header2, text)
                maps.area_status['toolroomRatpoison_taken'] = True
                for i in '123':
                    inventory().item_adder('poison')
                self.enter()

        elif((choice == 'inspect picture frame') or (choice == '2')):
            text = 'You looked at the picture frame and you saw a picture of a young guy and you saw the date indicated in the picture. It was taken 25 years ago.'
            misc().interfacer(text, None)
            self.enter()

        elif((choice == 'inspect toolbox') or (choice == '3')): #this is critical for the game flow later.
            text = 'You opened the toolbox and you got a variety of items.'
            item1 = 'hammer'
            item2 = 'pliers'
            item3 = 'screwdriver'
            text_header1 = f'[You obtained {item1}]{misc.spacer}[You obtained {item2}]{misc.spacer}[You obtained {item3}]'

            text_header2 = '[You already took the items]'

            if((maps('toolroomToolBox_opened', 'area_status').mapper()) == True):
                misc().interfacer(text_header2, None)
                self.enter()
            else:
                misc().interfacer(text, text_header1)
                inventory().item_adder(f'{item1}')
                inventory().item_adder(f'{item2}')
                inventory().item_adder(f'{item3}') #for extracting wires from appliances
                maps.area_status['toolroomToolBox_opened'] = True
                self.enter()

        elif((choice == 'open door') or (choice == '4')):
            passcodeLock(5, 10, 9, 25119, 'kitchen', 'GameOver', self, None).engage() # this is BIG

        elif((choice == 'go back') or (choice == '5')):
            garage().before_toolroom()

        elif((choice == 'check items') or (choice == '6')):
            inventory().check_items()
            self.enter()

        else:
            print(f'{misc.void}')
            self.enter()

class kitchen:

    def enter(self):
        text_header1 = f'[You are in the Kitchen Room]'
        text = f'[You are now in the kitchen]{misc.double_spacer}After you opened the door, you are lucky that there are no kidnappers there. You looked around and on your left there is a refrigerator with a cabinet beside it. There is also a toaster on top of the refrigerator. On your right there is a dog food tray and there is a door in front of you. The door has a mini door in it and that is where the dog comes in.{misc.spacer}What would you do?{misc.spacer}'
        choices = f'[(1) inspect cabinet] [(2) poison the dogfood tray] [(3) open the fridge] [(4) open the door] [(5) check items] [(6) go back] [(7) take toaster]'

        if(maps.area_status['kitchen_pass'] == True):
            misc().interfacer(text_header1, choices)
        else:
            misc().interfacer(text, choices)
            maps.area_status['kitchen_pass'] = True

        choice = input(f'{misc.prompt}')

        if((choice == 'inspect cabinet') or (choice == '1')):

            if(maps.area_status['kitchenCabinet'] == False):
                text_header1 = '[You obtained a knife]'
                text = 'As you open the cabinet, you saw a knife and took it for later use. The cabinet doesn\'t have much items in it so it can be used as a hiding place.'
                inventory().item_adder('knife')
                maps.area_status['kitchenCabinet'] = True
                misc().interfacer(text,text_header1)

            else:
                print('[You already took the knife]')

            self.enter()

        elif((choice == 'poison the dogfood tray') or (choice == '2')):

            if(maps.area_status['dogfoodPoison'] == True):
                text = 'You already killed the dog'
                misc().interfacer(text, None)
                self.enter()

            elif('poison' in inventory.items):
                text = 'You took one sachet of rat poison and you mixed it with dog food in the tray. You hid in the cabinet and waited. After a while, the dog came through the mini door and ate the dogfood with poison. After eating it, the dog started twiching and it is scratching its mouth. The poison has spread through its body and it died.'

                misc().interfacer(text, None)
                inventory().item_remover('poison')
                maps.area_status['dogfoodPoison'] = True
                self.enter()

            else:
                print('You have no poison')
                maps.area_status['kitchen_pass'] = True
                self.enter()

        elif((choice == 'open the fridge') or (choice == '3')):

            if(maps.area_status['napper1_dead'] == False):
                    text = f'As you open the fridge, you hear footsteps coming from outside of the kitchen. The footsteps seems to come in your direction. You looked in the fridge and you saw a food. {misc.spacer}What would you do?{misc.spacer}'
                    choices = '[(1) poison the food] [(2) hide in the cabinet]'
                    misc().interfacer(text, choices)
                    choice = input(f'{misc.prompt}')
                    maps.area_status['kitchenFridge'] = True

                    if((choice == 'poison the food') or (choice == '1')):

                        if('poison' in inventory.items):
                            maps.nappersNum -= 1
                            text = f'You used another sachet of rat poison and you put it in the food inside the fridge.{misc.spacer}After that, you quickly hid in the cabinet and you peeked through the small hole in the cabinet.{misc.spacer}The kidnapper  went inside the kitchen and opened the fridge and started eating. After a few minutes, the poison worked and the kidnapper started vomiting blood. The kidnapper died.{misc.spacer}'
                            text_header1 = f'[There are {maps.nappersNum} kidnappers left]'

                            misc().interfacer(text, text_header1)
                            inventory().item_remover('poison')
                            maps.area_status['napper1_dead'] = True
                        else:
                            print('You have no poison to use')

                    elif((choice == 'hide in the cabinet') or (choice == '2')):
                        print('You hid in the cabinet. The kidnapper opened the cabinet where you are in. He killed you on the spot.')
                        GameOver().enter()

                    else:
                        print(f'{misc.void}')
                        self.enter()
            else:
                text = 'You already poisoned the kidnapper'
                misc().interfacer(text, None)

            #maps.area_status['kitchen_pass'] = True
            self.enter()

        elif((choice == 'open the door') or (choice == '4')):
            if(maps.area_status['dogfoodPoison'] == False):
                text = 'The dog was alerted by your presence. It barked and the kidnappers started chasing you.'
                misc().interfacer(text,None)
                GameOver().enter()

            else:
                diningroom().enter()

        elif((choice == 'check items') or (choice == '5')):
            inventory().check_items()
            maps.area_status['kitchen_pass'] = True
            self.enter()

        elif((choice == 'toolroom') or (choice == '6')):
            toolroom().enter()

        elif((choice == 'take toaster') or (choice == '7')):
            if(maps.area_status['kitchenToaster'] == False):
                text_header1 = '[You obtained a toaster]'
                text = 'You reached and took the toaster from the top of the refrigerator'

                inventory().item_adder('toaster')
                maps.area_status['kitchenToaster'] = True
                misc().interfacer(text,text_header1)

            else:
                print('[You already took the toaster]')
            self.enter()

        else:
            print(f'{misc.void}')
            self.enter()

class diningroom:

    def enter(self):
        text_header1 = f'[You are in the Dining Room]'
        text = f'[You are now in the Dining Room]{misc.double_spacer}when you entered the dining room, you saw the dining table, chairs, door and a dead kidnapper.{misc.double_spacer}The dining table can serve as a crafting table.'
        choices = '[(1) search for items] [(2) take chair] [(3) open door] [(4) check items] [(5) crafting] [(6) go back]'

        if(maps.area_status['napper1_dead'] == True):

            if(maps.area_status['diningroom_pass'] == True):
                misc().interfacer(text_header1, choices)
            else:
                misc().interfacer(text, choices)
                maps.area_status['diningroom_pass'] = True

            choice = input(f'{misc.prompt}')

            if((choice == 'search for items') or (choice == '1')):

                if(maps.area_status['diningroom_grenadesTaken'] == False):
                    text = '[You obtained a smoke grenade]'
                    misc().interfacer(text, None)
                    maps.area_status['diningroom_grenadesTaken'] = True
                    inventory().item_adder('smokeGrenade')

                else:
                    text = '[You already searched for items.]'
                    misc().interfacer(text, None)
                self.enter()

            elif((choice == 'take chair') or (choice == '2')):
                if(maps.area_status['diningroom_chairTaken'] == False):
                    text = '[You obtained a chair]'
                    misc().interfacer(text, None)
                    inventory().item_adder('chair')
                    maps.area_status['diningroom_chairTaken'] = True

                else:
                    text = '[You already taken the chair]'
                    misc().interfacer(text, None)
                self.enter()

            elif((choice == 'open door') or (choice == '3')):
                corridor().enter()

            elif((choice == 'check items') or (choice == '4')):
                inventory().check_items()
                self.enter()

            elif((choice == 'crafting') or (choice == '5')):
                self.crafting()
                self.enter()
            elif((choice == 'go back') or (choice == '6')):
                kitchen().enter()
            else:
                print(f'{misc.void}')
                self.enter()

        else:
            GameOver().enter()

    def crafting(self):
        craftingItems = []
        text = f'The items you have are {inventory().itemNumber_displayer()}'
        text_footer1 = f'Enter the number of the item to be used.{misc.spacer}Enter C to craft and D when done crafting.'
        misc().interfacer(text, text_footer1)

        while True:
            choice = input(f'{misc.prompt}')

            if(choice in (inventory().inventory_LenCounter())):
                craftingItems.append(inventory.items[(int(choice)-1)])
                print(craftingItems)
            elif((choice == 'C') or (choice == 'c')):

                if((((('sleepingGrenade' in craftingItems) and ('poison' in craftingItems)) and (len(craftingItems) == 2))) or (((('poison' in craftingItems) and ('sleepMeds' in craftingItems) and ('smokeGrenade'in craftingItems)) and (len(craftingItems) == 3))) or (((('poisonGrenade' in craftingItems) and ('sleepMeds' in craftingItems)) and (len(craftingItems) == 2)))):
                    misc().interfacer('[You crafted a PoisonSleepingGrenade]', None)
                    inventory().list_subtractor(inventory.items, craftingItems)
                    inventory().item_adder('PoisonSleepingGrenade')

                elif((('smokeGrenade' in craftingItems) and ('poison' in craftingItems)) and (len(craftingItems) == 2)):
                    misc().interfacer('[You crafted a poisonGrenade]', None)
                    inventory().list_subtractor(inventory.items, craftingItems)
                    inventory().item_adder('poisonGrenade')

                elif((('sleepMeds' in craftingItems) and ('smokeGrenade' in craftingItems)) and (len(craftingItems) == 2)):
                    print('[You crafted a sleepingGrenade]')
                    inventory().list_subtractor(inventory.items, craftingItems)
                    inventory().item_adder('sleepingGrenade')

                elif((('screwdriver' in craftingItems) and ('toaster' in craftingItems)) and (len(craftingItems) == 2)):
                    misc().interfacer('[You got 2 wires]', None)
                    inventory().list_subtractor(inventory.items, craftingItems)
                    inventory().item_adder('wire')
                    inventory().item_adder('wire')
                    inventory().item_adder('screwdriver')

                elif((('hammer' in craftingItems) and ('toaster' in craftingItems)) and (len(craftingItems) == 2)):
                    misc().interfacer('[You got 2 wires]', None)
                    inventory().list_subtractor(inventory.items, craftingItems)
                    inventory().item_adder('wire')
                    inventory().item_adder('wire')
                    inventory().item_adder('hammer')

                elif((('pliers' in craftingItems) and ('wire' in craftingItems)) and (len(craftingItems) == 2)):
                    misc().interfacer('[You got lockpick]', None)
                    inventory().list_subtractor(inventory.items, craftingItems)
                    inventory().item_adder('lockpick')
                    inventory().item_adder('pliers')

                else:
                    misc().interfacer('[Crafting Failed]', None)
                self.crafting()
                break

            elif((choice == 'D') or (choice == 'd')):
                self.enter()
                break

            else:
                print('Not in inventory')
            continue

class corridor:

    def enter(self):
        text_header1 = '[You are now in the corridor]'
        text = 'You looked around you and there is a bathroom in front of you. You heard shower noise from it. There is a meds room on your left and the living room beside it.'
        choices = '[(1) inspect bathroom door] [(2) meds room] [(3) living room] [(4) check items] [(5) go back]'

        if(maps.area_status['corridor_pass'] == True):
            misc().interfacer(text_header1, choices)
        else:
            misc().interfacer((text_header1+'\n'+text), choices)
            maps.area_status['corridor_pass'] = True

        choice = input(f'{misc.prompt}')

        if((choice == 'inspect bathroom door') or (choice == '1')):
            text = 'You slowly walked toward the bathroom door and listened to the singing kidnapper inside the bathroom. He seems to be enjoying himself. You could use this opportunity.'
            text1 = 'You already rigged the bathroom'
            text2 = 'You already killed the kidnapper in the bathroom'
            choices = '[(1) rig the doorknob] [(2) open the door]'

            if(maps.area_status['bathroomRig'] == True):
                misc().interfacer(text1, None)
            elif(maps.nappersNum == 3):
                misc().interfacer(text2, None)
            else:
                misc().interfacer(text, choices)
                choice = input(f'{misc.prompt}')

                if((choice == '1') or (choice == "rig the doorknob")):

                    text_header1 = 'What item do you want to use?'
                    choices = f'The items you have are {inventory().itemNumber_displayer()}'

                    while True:
                        misc().interfacer(text_header1, choices)
                        choice = input(f'{misc.prompt}')
                        if(choice in (inventory().inventory_LenCounter())):

                                if(inventory.items[(int(choice)-1)] == 'chair'):
                                    text_header2 = '[You used the chair]'
                                    text = 'You placed the chair on the bathroom door. the kidnapper won\'t be able to get out now.'
                                    misc().interfacer(text_header2,text)
                                    inventory().item_remover('chair')
                                    maps.area_status['bathroomRig'] = True
                                    self.enter()
                                    break
                                else:
                                    misc().interfacer('You cannot use this', None)
                        else:
                            print(f'{misc.void}')
                        continue
                    self.enter()

                elif((choice == '2') or (choice == 'open the door')):
                    text_header1 = 'What item do you want to use?'
                    text_header2 = 'You already killed the kidnapper'
                    text = 'You slowly opened the bathroom door, you saw the kidnapper\'s face covered with soap. You can use this opportunity.'
                    choices = f'The items you have are {inventory().itemNumber_displayer()}'
                    misc().interfacer(text, None)

                    while(maps.nappersNum != 3):
                        misc().interfacer(text_header1, choices)
                        choice = input(f'{misc.prompt}')

                        if(choice in (inventory().inventory_LenCounter())):
                            corresponding_item = inventory.items[(int(choice)-1)]
                            text_header3 = f'[You used the {corresponding_item}]'

                            if((corresponding_item == 'knife') or (corresponding_item == 'hammer')):
                                text = 'You successfully killed the kidnapper'
                                inventory().item_remover(corresponding_item)
                                misc().interfacer(text, None)

                            else:
                                misc().interfacer(text_header3,text)
                                text = 'You failed to kill the kidnapper'
                                misc().interfacer(text, None)

                            maps.nappersNum -= 1
                            self.enter()
                        else:
                            print(f'{misc.void}')
                        continue
                self.enter()

        elif((choice == 'meds room') or (choice == '2')):
            text = 'You went to the meds room. It is locked by a simple padlock and it can be easily picked.'
            choices = '[(1) use lockpick] [(2) go back]'
            misc().interfacer(text, choices)
            choice = input(f'{misc.prompt}')

            if((choice == '1') or (choice == 'use lockpick')):
                if(inventory().checker('lockpick') == True):
                    inventory().item_remover('lockpick')
                    medsroom().enter()

                else:
                    text = 'You have no lockpick'
                    misc().interfacer(text, None)
                    self.enter()

        elif((choice == 'living room') or (choice == '3')):

            if(inventory().checker('key') == True):
                text = 'You already got the key'
                misc().interfacer(text, None)
                self.enter()
            else:
                text = 'You went to the living room. It is locked by a simple padlock and it can be easily picked.'
                choices = '[(1) use lockpick] [(2) go back]'
                misc().interfacer(text, choices)
                choice = input(f'{misc.prompt}')

                if((choice == '1') or (choice == 'use lockpick')):
                    if(inventory().checker('lockpick') == True):
                        inventory().item_remover('lockpick')
                        livingroom().enter()

                    else:
                        text = 'You have no lockpick'
                        misc().interfacer(text, None)
                        self.enter()

        elif((choice == 'check items') or (choice == '4')):
            inventory().check_items()
            self.enter()

        elif((choice == 'go back') or (choice == '5')):
            diningroom().enter()

        else:
            print(f'{misc.void}')
            self.enter()

class medsroom:

    def enter(self):
        text_header1 = '[You are in the meds room]'
        text = 'You entered the meds room and you did not find any bandages or medicine. It seems that those medicines were used up by the kidnappers. But you saw pack of sleeping pills with one pill left.'
        choices = '[(1) take sleeping pill] [(2) go back]'

        if(maps.area_status['medsroom_pass'] == False):
            misc().interfacer((text_header1+'\n'+text), choices)
            maps.area_status['medsroom_pass'] = True
        else:
            misc().interfacer(text_header1, choices)
        choice = input(f'{misc.prompt}')

        if((choice == 'take sleeping pill') or (choice == '1')):
            if(maps.area_status['medsroom_pillTaken'] != True):
                text = '[You took the sleeping pill]'
                inventory().item_adder('sleepMeds')
                misc().interfacer(text, None)
                maps.area_status['medsroom_pillTaken'] = True
                self.enter()
            else:
                text = 'You already took the sleeping pill'
                misc().interfacer(text, None)
                self.enter()

        elif((choice == 'go back') or (choice == '2')):
            corridor().enter()

class livingroom:

    def enter(self):
        text_header1 = '[You are in front of the Living Room]'
        text = f'As you aproach the door to the living room, you heard the sound of the television. You slowly opened the door lock and you saw {maps.nappersNum} kidnappers. They are just watching tv not knowing that you are just behind them. You could use this opportunity.'
        text_header3 = 'What item do you want to use?'
        choices = f'The items you have are {inventory().itemNumber_displayer()}'

        while True:
            if(maps.area_status['livingroomfront_pass'] != True):
                misc().interfacer((text_header1+f'{misc.double_spacer}'+text), choices)
                maps.area_status['livingroomfront_pass'] = True
            else:
                misc().interfacer(text_header1, choices)

            choice = input(f'{misc.prompt}')
            if(choice in (inventory().inventory_LenCounter())):

                    if(inventory.items[(int(choice)-1)] == 'PoisonSleepingGrenade'):
                        text_header2 = '[You used the PoisonSleepingGrenade]'
                        text = 'You silenty tossed the grenade in the living room and you closed the door shut. The kidnappers inhaled the sleeping medicine with the poison. They became unconsious and died.'
                        misc().interfacer(text_header2,text)
                        inventory().item_remover('PoisonSleepingGrenade')
                        maps.area_status['livingroom_grenade'] = True
                        self.after()
                        break
                    else:
                        misc().interfacer('You cannot use this', None)
            else:
                print(f'{misc.void}')
            continue
        self.enter()

    def after(self):
        text_header2 = '[You are in the Living Room]'
        item1 = 'clue'
        text_header3 = f'[You obtained {item1}]'
        text_header4 = '[You already seached the kidnappers]'
        text = 'You entered the living room and the dead kidnappers were lying dead on the floor. There is a safe that is unlocked through a combination code.'
        choices = '[(1) search the kidnappers] [(2) open the safe]'
        choices2 = '[(1) search the kidnappers] [(2) open the safe] [(3) read the clues]'

        if(maps.area_status['livingroom_cluesTaken'] == False):
            misc().interfacer((text_header2+f'{misc.double_spacer}'+text), choices)
        else:
            misc().interfacer((text_header2+f'{misc.double_spacer}'+text), choices2)

        choice = input(f'{misc.prompt}')

        if((choice == 'search the kidnappers') or (choice == '1')):
            if(maps.area_status['livingroom_cluesTaken'] != True):
                text = 'You searched the kidnappers for clues in opening the safe. You found these items:'
                misc().interfacer(text, text_header3)
                maps.area_status['livingroom_cluesTaken'] = True
                self.after()
            else:
                misc().interfacer(text_header4, None)
                self.after()

        elif((choice == 'open the safe') or (choice == '2')):
            passcodeLock(3, 10, 9, '052', 'corridor', 'livingroom', self, 'key').engage2()

        elif((choice == 'read the clues') or (choice == '3')):
            text = f'[6 8 2] One number is correct and well placed.{misc.spacer}[6 4 5] One number is correct but wrong placed.{misc.spacer}[2 0 6] Two numbers are correct but wrong placed.{misc.spacer}[7 3 8] Nothing is correct.{misc.spacer}[7 8 0] One number is correct but wrong placed.'
            misc().interfacer(text, None)
        else:
            print(f'{misc.void}')
        self.after()

class GameOver:

    def enter(self):

        if((maps.area_status['good_ending']) == True):
            misc().interfacer('You reached the good ending', None)
            exit()
        else:
            misc().interfacer('You were caught and killed by the kidnappers', None)

        text = 'Try again?'
        choices = '[(1) yes] [(2) no]'
        misc().interfacer(text, choices)
        choice = input(misc.prompt)

        if((choice == 'yes') or (choice == '1')):
            # This is where the intro screen should come in, but we'll use the garage for the mean time.
            inventory.items = []
            maps.area_status = {
            'garage_pass' : False,
            'garageLock_open' : False,
            'toolroom_pass' : False,
            'toolroomToolBox_opened' : False,
            'toolroomRatpoison_taken' : False,
            'toolroomCombinationLock_opened' : False,
            'kitchen_pass' : False,
            'kitchenFridge' : False,
            'kitchenCabinet' : False,
            'kitchenCabinet_hide' : False,
            'kitchenToaster' : False,
            'dogfoodPoison' : False,
            'diningroom_pass' : False,
            'diningroom_grenadesTaken' : False,
            'diningroom_chairTaken' : False,
            'medsroom_pass' : False,
            'medsroom_pillTaken' : False,
            'livingroom_pass' : False,
            'livingroomfront_pass' : False,
            'livingroom_grenade' : False,
            'livingroom_cluesTaken' : False,
            'napper1_dead' : False,
            'napper2_dead' : False,
            'bathroomRig' : False,
            'corridor_pass' : False,
            'good_ending' : False
            }
            garage().enter()
        else:
            exit()
#-------------------------------------------------------------------------------------------------------------------------------------------------

class maps(object): # maps , area_status are the modes

    nappersNum = 5

    all_maps = {
    'garage': garage(),
    'toolroom': toolroom(),
    'kitchen': kitchen(),
    'diningroom': diningroom(),
    'medsroom': medsroom(),
    'livingroom': livingroom(),
    'corridor': corridor(),
    'GameOver': GameOver()
    }

    area_status = {
    'garage_pass' : False,
    'garageLock_open' : False,
    'toolroom_pass' : False,
    'toolroomToolBox_opened' : False,
    'toolroomRatpoison_taken' : False,
    'toolroomCombinationLock_opened' : False,
    'kitchen_pass' : False,
    'kitchenFridge' : False,
    'kitchenCabinet' : False,
    'kitchenCabinet_hide' : False,
    'kitchenToaster' : False,
    'dogfoodPoison' : False,
    'diningroom_pass' : False,
    'diningroom_grenadesTaken' : False,
    'diningroom_chairTaken' : False,
    'medsroom_pass' : False,
    'medsroom_pillTaken' : False,
    'livingroom_pass' : False,
    'livingroomfront_pass' : False,
    'livingroom_grenade' : False,
    'livingroom_cluesTaken' : False,
    'napper1_dead' : False,
    'napper2_dead' : False,
    'bathroomRig' : False,
    'corridor_pass' : False,
    'good_ending' : False
    }

    def __init__(self, mapstring, mode):
        self.mapstring = mapstring
        self.mode = mode

    def mapper(self):

        if(self.mode == 'maps'):
            mapname = self.all_maps.get(self.mapstring)
            return mapname

        elif(self.mode == 'area_status'):
            statusValue = self.area_status.get(self.mapstring)
            return statusValue

a = garage()
a.enter()
