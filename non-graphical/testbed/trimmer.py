# pretty much the same as alphabetical just excluding the cut_list
from sys import argv

script, filename = argv
target = open(filename, 'r+')

lines_list = target.read().splitlines()  # loads the text lines into list form

upperLimit = int(input('Enter the upper limit: '))-1 # should input line after protected
lowerLimit = int(input('Enter the lower limit: '))  # should input line before protected line

upper_list = lines_list[:upperLimit] 
lower_list = lines_list[lowerLimit:] 

#cut_list = lines_list[upperLimit:lowerLimit]
#cut_list.sort()

target = open(filename, 'w+')
target.truncate()

def writer(listType):
    for i in range(len(listType)):
        target.write(listType[i])
        target.write('\n')

writer(upper_list)
#writer(cut_list)
writer(lower_list)